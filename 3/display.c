#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>

#include <wiringPi.h>
#include <wiringPiI2C.h>

#define DATAPIN 21

bool requestDHT();
bool checksum();
void downTrigger();
void clearBytes();
void textCommand(unsigned char cmd);
int setText(const char *string);

int timediff, before, bit;
int byte[5] = {0, 0, 0, 0, 0};

// I2C device handles.
int backLight;
int textDisplay;

// Internal use only.
void textCommand(unsigned char cmd)
{
    wiringPiI2CWriteReg8(textDisplay, 0x80, cmd);
}

// Set text of diaplay.
int setText(const char *string)
{
  // Clear display.
  textCommand(0x01);
  
  delay(50);
  
  // Display on, no cursor.
  textCommand(0x08 | 0x04);
  
  // Display two lines.
  textCommand(0x28);
  
  delay(50);
  
  int count = 0;
  int row = 0;
  int i = 0;
  
  for (; string[i] != '\0'; ++i) {
    if ((string[i] == '\n') || (count == 16)) {
      count = 0;
      ++row;
      if (row == 2) {
	// Reach maximum line number. Truncate any characters behind.
	break;
      }
      textCommand(0xc0);
            if (string[i] == '\n') {
	      continue;
	    }
    }
    ++count;
    wiringPiI2CWriteReg8(textDisplay, 0x40, string[i]);
  }
}

bool requestDHT()
{
  // Set pin mode to OUTPUT
  pinMode(DATAPIN, OUTPUT);

  // Reset bit counter to 0
  bit = 0;
  clearBytes();
 
  // Pull down for 18ms and pull up for 30 μs to initiate data sequence
  digitalWrite(DATAPIN, LOW);
  delay(18);
  digitalWrite(DATAPIN, HIGH); 
  delayMicroseconds(30);

  // Set pin mode to INPUT
  pinMode(DATAPIN, INPUT);

  // Get first time before reading
  before = micros();

  // Wait for data and determine if valid
  delay(20);
  return checksum();
}

bool checksum()
{
  // Sum of bits 0-3 should equal bit 4
  // bytes cannot exceed values beyond 0 and 255
  if (byte[4] == (byte[0] + byte[1] + byte[2] + byte[3]) && byte[4] < 255 && byte[4] > 0)
    {
      return true;
    }

  else
    {
      return false;
    }
}

void clearBytes()
{
  for (int i = 0; i < 5; i ++)
	byte[i] = 0;
}

void downTrigger()
{
  // Get time difference in microseconds
  timediff = micros() - before;

  // Ignore first 3 bit readings
  // If the time is longer than 100 μs, it's a 1
  if (bit > 2 && (timediff > 100))
    {
      // Shift and increment
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
      byte[ (bit-3) / 8 ]++;
    }

  // Otherwise, it must be a 0
  else if (bit > 2)
    {
      // Shift only
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
    }

  // Increment bit counter
  bit++;

  // Get relative time for next reading
  before = micros();
}

// Signal handler.
void sigIntHandler(int signal)
{
  // Clear screen.
  textCommand(0x01);
  
  printf("\nProgram ended with exit value -2.\n");
  exit(-2);
}

int main(int argc, const char *argv[])
{
  // Catch signal (Ctrl + C).
  struct sigaction sigHandler;
  sigHandler.sa_handler = sigIntHandler;
  sigemptyset(&sigHandler.sa_mask);
  sigHandler.sa_flags = 0;
  sigaction(SIGINT, &sigHandler, NULL);
	
  // Setup wiringPi.
  wiringPiSetupGpio();
  
  // Setup I2C bus.
  // Backlight I2C address = 0x62
  // Text display I2C address = 0x3E
  backLight = wiringPiI2CSetup(0x62);
  textDisplay = wiringPiI2CSetup(0x3E);

  // Call downTrigger() on falling edge interrupt
  wiringPiISR(DATAPIN, INT_EDGE_FALLING, &downTrigger);

  // Default pin to HIGH
  pullUpDnControl(DATAPIN, PUD_UP);
  
  if((backLight == -1) || (textDisplay == -1)) {
    fprintf(stderr, "Failed to initialize I2C device. Exit.\n");
    printf("Program ended with exit value -1.\n");
    return -1;
  }
  
  // Display text.
  //setText("Hello:\n      World!");

  char displaymessage[32];
  while (true)
    {
      int count = 0;
      do  { count++; } while ( count < 20 && requestDHT() == false);
      
      // Err if compromised
      if (count == 20)
	{
	  printf("WARNING: DHT11 could not read data.\n");
	  continue;
	}

      sprintf(&displaymessage[0], "H: %i.%i\nT: %i.%i", byte[0], byte[1], byte[2], byte[3]);
      //printf(displaymessage);
      setText(displaymessage);
      delay(5000);
    }
}
