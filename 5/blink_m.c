int led = 12; // Changed to 12

void setup() {                
  pinMode(led, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT); // Internal LED
}

void loop() {
  digitalWrite(led, HIGH);
  digitalWrite(LED_BUILTIN, LOW); // Write LOW when external HIGH
  delay(1000);
  digitalWrite(led, LOW);
  digitalWrite(LED_BUILTIN, HIGH); // Write HIGH when external LOW
  delay(1000);
}
