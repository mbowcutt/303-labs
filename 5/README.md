# Lab 5 - Introduction to Arduino

## Files

Modified Examples
* [blink_m.c](blink_m.c)
* [blink_no_delay_m.c](blink_no_delay_m.c)
* [digital_read_serial_m.c](digital_read_serial_m.c)

Example Code
* [blink.c](examples/blink.c)
* [blink_no_delay.c](examples/blink_no_delay.c)
* [digital_read_serial.c](examples/digital_read_serial.c)