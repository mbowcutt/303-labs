int pushButton = 10; // Input Pin
int led = 12;        // LED Output Pin
int laststate;       // Last state of LED Output

void setup() {
  Serial.begin(9600);
  pinMode(pushButton, INPUT); // Input
  pinMode(led, OUTPUT);       // Output
}

void loop() {
  int buttonState = digitalRead(pushButton);
  Serial.println(buttonState);

  if (buttonState != laststate)     // Write output if different
  {
    digitalWrite(led, buttonState);  
    laststate = buttonState;
  }

  delay(1);
}
