#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wiringPi.h>

#define DATAPIN 21

bool requestDHT();
void readPoll();
bool checksum();
void clearBytes();

int byte[5] = {0, 0, 0, 0, 0};

int main(void)
{
  // Set up board
  wiringPiSetupGpio();

  // Default pin to HIGH
  pullUpDnControl(DATAPIN, PUD_UP);

  // Loop until read success
  int count = 0;
  do  { count++; } while ( count < 20 && requestDHT() == false);

  // Err if compromised
  if (count == 20)
    {
      fprintf(stderr, "Could not read data.\n");
      exit(EXIT_FAILURE);
    }

  // Print formatted data
  printf("After %i attempt(s), the DHT11 Sensor read:\n", count);
  printf("Relative Humidity: \t%i.%i%\n", byte[0], byte[1]);
  printf("Temperature: \t\t%i.%i°C\n", byte[2], byte[3]);
  exit(EXIT_SUCCESS);
}

bool requestDHT()
{
  // Set pin mode to OUTPUT
  pinMode(DATAPIN, OUTPUT);

  // Pull down for 18ms and pull up for 30 μs to initiate data sequence
  digitalWrite(DATAPIN, LOW);
  delay(18);
  digitalWrite(DATAPIN, HIGH);
  delayMicroseconds(20);

  // Set pin mode to INPUT
  pinMode(DATAPIN, INPUT);

  // Read data and validate it
  readPoll();
  return checksum();
}

void readPoll()
{
  // Set bit counter to 0
  int bit = 0;
  clearBytes();

  // Read 41 bits total
  while (bit < 41)
    {
      // Get the before and after state and time
      int before_state = digitalRead(DATAPIN);
      int before_time = micros();
      int after_state, after_time;

      // Limit to catch us from an infinite loop of LOW
      int limit = 0;

      // Loop until the state has changed
      while (after_state = digitalRead(DATAPIN), limit++ < 255 && after_state == before_state)
	{
	  delayMicroseconds(1);
	}

      // Get the time difference
      int timediff = micros()-before_time;

      // If we have just returned from on HIGH
      if (before_state == 1)
	{
	  // Ignore the first bit
	  // If time is longer than 35 μs it is a 1
	  if (bit > 0 && timediff > 35)
	    {
	      // Shift and increment
	      byte[(bit-1) / 8] = byte[(bit-1) / 8] << 1;
	      byte[(bit-1) / 8]++;
	    }
	  // Otherwise it is a 0
	  else if (bit > 0)
	    {
	      // Increment only
	      byte[(bit-1) / 8] = byte[(bit-1) / 8] << 1;
	    }
	  // Increment bit counter
	  bit++;
	}
    }
}

bool checksum()
{
  // Sum of bits 0-3 should equal bit 4
  // bytes cannot exceed values beyond 0 and 255
  if (byte[4] == (byte[0] + byte[1] + byte[2] + byte[3]) && byte[4] < 255 && byte[4] > 0)
    {
      return true;
    }

  else
    {
      return false;
    }
}

void clearBytes()
{
  for (int i = 0; i < 5; i ++)
        byte[i] = 0;
}

