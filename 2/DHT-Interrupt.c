#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wiringPi.h>

#define DATAPIN 21

bool requestDHT();
bool checksum();
void downTrigger();
void clearBytes();

int timediff, before, bit;
int byte[5] = {0, 0, 0, 0, 0};

int main()
{
  // Set up board
  wiringPiSetupGpio();

  // Call downTrigger() on falling edge interrupt
  wiringPiISR(DATAPIN, INT_EDGE_FALLING, &downTrigger);

  // Default pin to HIGH
  pullUpDnControl(DATAPIN, PUD_UP);

  // Loop until read success
  int count = 0;
  do  { count++; } while ( count < 20 && requestDHT() == false);

  // Err if compromised
  if (count == 20)
    {
      fprintf(stderr, "Could not read data.\n");
      exit(EXIT_FAILURE);
    }

  // Print formatted data
  printf("After %i attempt(s), the DHT11 Sensor read:\n", count);
  printf("Relative Humidity: \t%i.%i%\n", byte[0], byte[1]);
  printf("Temperature: \t\t%i.%i°C\n", byte[2], byte[3]);
  exit(EXIT_SUCCESS);
}

bool requestDHT()
{
  // Set pin mode to OUTPUT
  pinMode(DATAPIN, OUTPUT);

  // Reset bit counter to 0
  bit = 0;
  clearBytes();
 
  // Pull down for 18ms and pull up for 30 μs to initiate data sequence
  digitalWrite(DATAPIN, LOW);
  delay(18);
  digitalWrite(DATAPIN, HIGH); 
  delayMicroseconds(30);

  // Set pin mode to INPUT
  pinMode(DATAPIN, INPUT);

  // Get first time before reading
  before = micros();

  // Wait for data and determine if valid
  delay(20);
  return checksum();
}

bool checksum()
{
  // Sum of bits 0-3 should equal bit 4
  // bytes cannot exceed values beyond 0 and 255
  if (byte[4] == (byte[0] + byte[1] + byte[2] + byte[3]) && byte[4] < 255 && byte[4] > 0)
    {
      return true;
    }

  else
    {
      return false;
    }
}

void clearBytes()
{
  for (int i = 0; i < 5; i ++)
	byte[i] = 0;
}

void downTrigger()
{
  // Get time difference in microseconds
  timediff = micros() - before;

  // Ignore first 3 bit readings
  // If the time is longer than 100 μs, it's a 1
  if (bit > 2 && (timediff > 100))
    {
      // Shift and increment
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
      byte[ (bit-3) / 8 ]++;
    }

  // Otherwise, it must be a 0
  else if (bit > 2)
    {
      // Shift only
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
    }

  // Increment bit counter
  bit++;

  // Get relative time for next reading
  before = micros();
}
