#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <wiringPi.h>

#define DATAPIN 21
#define LEDPIN 20
#define CUTOFFTEMP 23

bool requestDHT();
bool checksum();
void downTrigger();
void printBits();
void clearBits();
void clearBytes();

int timediff, before, bit;
int byte[5] = {0, 0, 0, 0, 0};
int bits[40];

int main()
{
  // Set up board
  wiringPiSetupGpio();

  // Call downTrigger() on falling edge interrupt
  wiringPiISR(DATAPIN, INT_EDGE_FALLING, &downTrigger);
  pinMode(LEDPIN, OUTPUT);
  digitalWrite(LEDPIN, LOW);

  // Default pin to HIGH
  pullUpDnControl(DATAPIN, PUD_UP);

  while (true)
    {
      // Loop until read success
      int count = 0;
      do  { count++; } while (count < 20 && requestDHT() == false);

      // Err if compromised
      if (count < 20)
        {
	  if (digitalRead(LEDPIN) && byte[2] < CUTOFFTEMP)
	    {
	      digitalWrite(LEDPIN, LOW);
            }
          if (!digitalRead(LEDPIN) && byte[2] > CUTOFFTEMP)
            {
              digitalWrite(LEDPIN, HIGH);
            }
	printf("Temperature read: %i\n", byte[2]);
        }
      else { printf("Error:\n"); }
      //printBits();
    }
}

bool requestDHT()
{
  // Set pin mode to OUTPUT
  pinMode(DATAPIN, OUTPUT);

  // Reset bit counter to 0
  bit = 0;
  clearBits();
  clearBytes();

  // Pull down for 18ms and pull up for 30 μs to initiate data sequence
  digitalWrite(DATAPIN, LOW);
  delay(18);
  digitalWrite(DATAPIN, HIGH);
  delayMicroseconds(30);

  // Set pin mode to INPUT
  pinMode(DATAPIN, INPUT);

  // Get first time before reading
  before = micros();

  // Wait for data and determine if valid
  delay(20);
//  printBits();
  return checksum();
}

bool checksum()
{
  // Sum of bits 0-3 should equal bit 4
  // bytes cannot exceed values beyond 0 and 255
  if (byte[4] == (byte[0] + byte[1] + byte[2] + byte[3]) && byte[4] < 255 && byte[4] > 0)
    {
      printf("Checksum looks good and is %i\n", byte[4]); 
      return true;
    }

  else
    {
      return false;
    }
}

void downTrigger()
{
  // Get time difference in microseconds
  timediff = micros() - before;

  // Ignore first 3 bit readings
  // If the time is longer than 100 μs, it's a 1
  if (bit > 2 && (timediff > 100))
    {
      // Shift and increment
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
      byte[ (bit-3) / 8 ]++;
      bits[bit-3] = 1;
    }

  // Otherwise, it must be a 0
  else if (bit > 2)
    {
      // Shift only
      byte[ (bit-3) / 8 ] = byte[ (bit-3) / 8 ] << 1;
      bits[bit-3] = 0;
    }

  // Increment bit counter
  bit++;

  // Get relative time for next reading
  before = micros();
}

void printBits()
{
	printf("%i, %i, %i, %i, %i, %i, %i, %i\n", bits[0], bits[1], bits[2], bits[3], bits[4], bits[5], bits[6], bits[7]);
	printf("%i, %i, %i, %i, %i, %i, %i, %i\n", bits[8], bits[9], bits[10], bits[11], bits[12], bits[13], bits[14], bits[15]);
	printf("%i, %i, %i, %i, %i, %i, %i, %i\n", bits[16], bits[17], bits[18], bits[19], bits[20], bits[21], bits[22], bits[23]);
	printf("%i, %i, %i, %i, %i, %i, %i, %i\n", bits[24], bits[25], bits[26], bits[27], bits[28], bits[29], bits[30], bits[31]);
	printf("%i, %i, %i, %i, %i, %i, %i, %i\n", bits[32], bits[33], bits[34], bits[35], bits[36], bits[37], bits[38], bits[39]);
}

void clearBits()
{
  for (int i = 0; i < 40; i++)
	bits[i]=0;
}

void clearBytes()
{
  for (int i = 0; i < 5; i++)
	byte[i] = 0;
}
