	.globl main 		// Set Environment

main:
	mov r0, #0		// Set pin arg
	bl init_LED		// Init LED at pin 0

	mov r5, #99		// next intensity == 99

decrease:
	mov r0, #0		// Set pin arg
	mov r1, r5		// Set intensity arg
	bl set_LED		// Set intensity

	cmp r0, #1
	beq exit		// Exit if ERROR
	
	cmp r5, #0		// Branch to increase
	beq increase		// if intensity = 0

	sub r5, r5, #1		// else decrement intensity 1
	b decrease		// decrease

increase:
	mov r0, #0		// Set pin arg
	mov r1, r5		// Set intensity arg
	bl set_LED		// Set intensity

	cmp r0, #1
	beq exit		// Exit if ERROR
	
	cmp r5, #100		// Branch to decrease
	beq decrease		// if intensity = 100

	add r5, r5, #1		// else increment intensity 1
	b increase		// increase

exit:	
