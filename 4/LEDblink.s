	.globl main		// Set Environment	
	
main:
	mov r0, #0		// Initialize board and
				// GPIO Pin 0 as OUTPUT
	bl init_LED		// Branch to init(r0)

	cmp r0, #1
	beq exit		// Exit if ERROR
	
	mov r5, #0		// Init count to 0

loop:
	cmp r5, #10		// Compare count and 10
	beq exit		// Branch to exit
				// if count = 10
	
	mov r0, #0 		// Turn on LED at pin 0
	bl set_LED_high		// Branch to ledon(r0)

	cmp r0, #1
	beq exit		// Exit if ERROR

	mov r0, #0		// Turn off LED at pin 0
	bl set_LED_low		// Branch to ledoff(r0)

	cmp r0, #1
	beq exit		// Exit if ERROR
	
	add r5, r5, #1		// Increment count

	b loop			// Branch to top of loop
	
exit:				// Exit
