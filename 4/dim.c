#include <wiringPi.h>
#include <softPwm.h>

#define SUCCESS 0
#define ERROR 1

extern int init_LED(int pin)
{
  if(wiringPiSetup() < SUCCESS)
    return ERROR;
  if (softPwmCreate(pin, 100, 100) < SUCCESS)
    return ERROR;
  delay(20);
  
  return SUCCESS;
}

extern int set_LED(int pin, int value)
{
  softPwmWrite(pin, value);
  delay(20);

  return SUCCESS;
}
