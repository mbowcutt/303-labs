#include <wiringPi.h>

#define SUCCESS 0
#define ERROR 1

extern int init_LED(int pin)
{
  if (wiringPiSetup() < SUCCESS)
    return ERROR;
  pinMode(pin, OUTPUT);

  return SUCCESS;
}

extern int set_LED_high(int pin)
{
  digitalWrite(pin, HIGH);
  delay(250);

  return SUCCESS;
}

extern int set_LED_low(int pin)
{
  digitalWrite(pin, LOW);
  delay(250);

  return SUCCESS;
}
