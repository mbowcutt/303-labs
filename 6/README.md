# EECS 303 Lab 6

Traffic Controller assignment

# Files

* [Traffic Controller](TrafficController/TrafficController.ino)
* [Traffic Controller with Pedestrian Signals](PedestrianController/PedestrianController.ino)
* [Lab Assignment - Part 1](assign6.pdf)
* [Lab Assignment - Part 2](pedestrian_control.pdf)
* [Lab Report](report.pdf)

