const int aRed = 11;
const int aGreen = 9;
const int bRed = 12;
const int bGreen = 10;
const int buttonpin = 8;
const int pedestrianAbutton = 2;
const int pedestrianAled = 4;
const int pedestrianBbutton = 3;
const int pedestrianBled = 13;

int state = 1;
int buttonflag = false;
int time, lasttime;

boolean reqA = false;
boolean reqB = false;

void setup() {
  pinMode(aRed, OUTPUT);
  pinMode(aGreen, OUTPUT);
  pinMode(bRed, OUTPUT);
  pinMode(bGreen, OUTPUT);
  pinMode(buttonpin, INPUT);
  pinMode(pedestrianAbutton, INPUT);
  pinMode(pedestrianBbutton, INPUT);
  pinMode(pedestrianAled, OUTPUT);
  pinMode(pedestrianBled, OUTPUT);
  lasttime = millis();
  
  Serial.begin(9600);
}

void loop() {
  // Maintenance (flashing RED) logic
  if(buttonflag)
    {
      time = millis();
      if(time - lasttime > 1000)
	{
	  digitalWrite(aRed, HIGH);
	  digitalWrite(bRed, HIGH);
	  lasttime = millis();
	}
      else if (time - lasttime > 500)
	{
	  digitalWrite(aRed, LOW);
	  digitalWrite(bRed, LOW);
	}
      else
	{
	  digitalWrite(aRed, HIGH);
	  digitalWrite(bRed, HIGH);
	}
    }
  
  // State Switch logic
  else
    {
      switch(state)
	{
	case(1): // RED & RED 1000ms
	  time = millis();
	  
	  digitalWrite(aRed, HIGH);
	  digitalWrite(aGreen, LOW);
	  digitalWrite(bRed, HIGH);
	  digitalWrite(bGreen, LOW);
	  
	  if(time - lasttime > 1000)
	    {
	      state = 2;
              if(reqA)
                digitalWrite(pedestrianAled, HIGH);
	      lasttime = millis();
	    }
	  break;
	  
	case(2): // GREEN & RED 5000ms
	  time = millis();
	  
	  digitalWrite(aRed, LOW);
	  digitalWrite(aGreen, HIGH);
	  digitalWrite(bRed, HIGH);
	  digitalWrite(bGreen, LOW);
	  
	  if(time - lasttime > 5000)
	    {
	      state = 3;
	      lasttime = millis();
	    }
	  break;
	  
	case(3): // DIM & RED 1000ms
	  time = millis();
					
	  digitalWrite(aRed, LOW);
	  analogWrite(aGreen, 122);
	  digitalWrite(bRed, HIGH);     
	  digitalWrite(bGreen, LOW);
	  
	  if(time - lasttime > 1000)
	    {
	      state = 4;
              if(digitalRead(pedestrianAled))
              {
                digitalWrite(pedestrianAled, LOW);
                reqA = false;
              }
	      lasttime = millis();
	    }
	  break;
	  
	case(4): // RED & RED 1000ms
	  time = millis();
	  
	  digitalWrite(aRed, HIGH);
	  digitalWrite(aGreen, LOW);
	  digitalWrite(bRed, HIGH);
	  digitalWrite(bGreen, LOW);
	  
	  if(time - lasttime > 1000)
	    {
	      state = 5;
              if(reqB)
                digitalWrite(pedestrianBled, HIGH);
	      lasttime = millis();
	    }
	  break;
	  
	case(5): // RED & GREEN 5000ms
	  time = millis();
	  
	  digitalWrite(aRed, HIGH);
	  digitalWrite(aGreen, LOW);
	  digitalWrite(bRed, LOW);
	  digitalWrite(bGreen, HIGH);
	  
	  if(time - lasttime > 5000)
	    {
	      state = 6;
	      lasttime = millis();
	    }
	  break;
	  
	case(6): // RED & DIM 1000ms
	  time = millis();
	  
	  digitalWrite(aRed, HIGH);
	  digitalWrite(aGreen, LOW);
	  digitalWrite(bRed, LOW);
	  analogWrite(bGreen, 122);
	  
	  if(time - lasttime > 1000)
	    {
	      state = 1;
              if(digitalRead(pedestrianBled))
              {
                digitalWrite(pedestrianBled, LOW);
                reqB = false;
              }
	      lasttime = millis();
	    }
	  break;
	  
	default:
	  break;
	}
    }  
    if(digitalRead(buttonpin))
	{
	  buttonflag = !buttonflag;
	  delay(500);
          digitalWrite(aGreen, LOW);
          digitalWrite(bGreen, LOW);
	  lasttime=millis();
	}
    if(digitalRead(pedestrianAbutton))
      reqA = true;
    if(digitalRead(pedestrianBbutton))
      reqB = true;
}

