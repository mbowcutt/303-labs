# CWRU EECS 303 - Embedded Systems Lab Assignments

This is the code repository for the lab course on embedded systems taught by Professor [Christos Papachristou](mailto:cap2@case.edu) and [Paul Whitten](pcw@case.edu) at Case Western Reserve University during the Fall 2018 term.

## Lab Members

* [Michael Bowcutt](mailto:mbowcutt@case.edu)
* [Jeffrey Shen](mailto:jxs1206@case.edu)

# Assignments

Note that as the term is currently in session, work on these assignments is ongoing.

## [1: Introduction to GPIO Pins](1/)

Using a Raspberry Pi, blink an LED circuit wired to the GPIO pins with code in C and Python.

## [2: Introduction to Sensors](2/)

Read the temperature and relative humidity from a DHT11 sensor.

## [3: LCD Display Assignment](3/)

Read the temperature and relative humidity and send the output to an LCD display.

## [4: Introduction to ARM](4/)

Compile ARM code with external procedures written in C that interface with the Raspberry Pi's GPIO Pins.

## [5: Introdution to Arduino](5/)

Connect to an Arduino from a Raspberry Pi and interface with the Arduino's GPIO pins.

## [k: Introduction to Kernel Modules](k/)

Compile C code into Linux kernel modules and enable them.

## [6: Traffic Controller Assignment](6/)

Create a traffic light timing pattern with pedestrian walk requests and on/off input using the Arduino.