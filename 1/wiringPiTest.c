#include <stdio.h>
#include <wiringPi.h>

const int ledPin=21;

int main(void)
{
	wiringPiSetupGpio();
	pinMode(ledPin, OUTPUT);

	while(1<2)
	{
		printf("Turned on.\n");
		digitalWrite(ledPin, HIGH);
		delay(75);
		printf("Turned off.\n");
		digitalWrite(ledPin, LOW);
		delay(75);
	}

	return 0;
}
