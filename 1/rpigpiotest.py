import RPi.GPIO as GPIO
import time

LedPin = 36

def setup():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setup(LedPin, GPIO.OUT)
	GPIO.output(LedPin, GPIO.HIGH)

def loop():
	while(True):
		GPIO.output(LedPin, GPIO.LOW)
		print 'led is off'
		time.sleep(0.5)
		GPIO.output(LedPin, GPIO.HIGH)
		print 'led is on'
		time.sleep(0.5)

def destroy():
	# TURN LED OFF
	GPIO.output(LedPin, GPIO.LOW)
	# Release resource
	GPIO.cleanup()

if __name__ == '__main__':
	setup()
	try:
		loop()
	except KeyboardInterrupt:
		destroy()
	
